package assignment2AI;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class xmlParse {
	Scanner sc2 = null;
	
	BinaryTree parse(){
		BinaryTree root=new BinaryTree();
	    try {
	        sc2 = new Scanner(new File("myXml.xml"));
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();  
	    }
	    return makeTree(root);
	}
	
	BinaryTree makeTree(BinaryTree b){
		while(sc2.hasNextLine()) {
			
			String line=sc2.nextLine();
			if(line.equals("")) continue;
			int isClosingTag=findString(line,"</",1);
			if(isClosingTag>=0){//its a closing tag
				continue;
			}
			
			
			String behavior=getBehavior(line);
			if(!behavior.equals("")){
				addBehavior(b,behavior);
			}else{
				assert(b.getData().equals(""));
				int first=findString(line,'<',1);
				int second=findString(line,'>',1);
				assert(first>=0 && second>=0);
				b.data=line.substring(first+1, second);
			}
		}
		return b;
	}
	
	//add behavior
	void addBehavior(BinaryTree b, String behavior){
		BinaryTree child=new BinaryTree();
		child.setData(behavior);
		
		while(sc2.hasNextLine()){
			String line=sc2.nextLine();
			
			String childResponse=getResponse(line);
			String childBehavior=getBehavior(line);
			
			if(!childBehavior.equals("")){
				addBehavior(child,childBehavior);
			}else if(!childResponse.equals("")){
				addResponse(child,childResponse);
			}else{
				break;
			}
			
		}
		
		b.addChild(child);
	}
	
	//add response
	void addResponse(BinaryTree b, String response){
		BinaryTree child=new BinaryTree();
		child.setData(response);
		b.addChild(child);
	}

	//gets behavior from the line
	String getBehavior(String line){
		String behavior="";
		int first=findString(line, '"',1);
		int second=findString(line, '"',2);
		if(first>=0){
			behavior=line.substring(first+1, second);
		}
		return behavior;
	}
	
	//gets response from the line
	String getResponse(String line){
		String respons="";
		int first=findString(line, '"',3);
		int second=findString(line, '"',4);
		if(first>=0){
			respons=line.substring(first+1, second);
		}
		return respons;
	}
	
	void printTree(BinaryTree header,int tab){
		for(int i=tab; i>0; i--){
			System.out.print("\t");
		}
		if(header.totalNumberOfChildren()>0){
			System.out.println("behavior = "+header.getData());
		}else{
			System.out.println("response = "+header.getData());
		}
		
		for(int i=0; i<header.totalNumberOfChildren(); i++){
			printTree(header.getChild(i),tab+1);
		}
	}
	
	//gets position 
	public int findString(String line, char c, int n) {
	    int pos = line.indexOf(c, 0);
	    while (--n > 0 && pos != -1)
	        pos = line.indexOf(c, pos+1);
	    return pos;
	}
	
	public int findString(String line, String s, int n) {
	    int pos = line.indexOf(s, 0);
	    while (--n > 0 && pos != -1)
	        pos = line.indexOf(s, pos+1);
	    return pos;
	}
}
