package assignment2AI;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.Scanner;

public class game {
	BinaryTree tree;
	int dfsSteps;
	int bfsSteps;
	Queue<BinaryTree> bfsQ= new LinkedList<BinaryTree>();
	
	game(){
		xmlParse p = new xmlParse();
		System.out.println("Reading xml file and loading Tree into memory...");
		tree = p.parse();
		System.out.println("Tree Loaded...");
		p.printTree(tree, 0);
		System.out.println("######################################################");
	}
	
	void startGame(){
		assert(tree.isempty());
		
		Scanner reader = new Scanner(System.in);  // Reading from System.in
		String input;
		System.out.print("'quit' to exit: ");
		input = reader.nextLine();
		do{
			dfsSteps=0;
			bfsSteps=0;
			if(input.equalsIgnoreCase("exit") || input.equalsIgnoreCase("quit")) break;
			
			BinaryTree dfsTree= searchWithDFS(tree,input);
			bfsQ.clear();
			bfsQ.add(tree);
			searchWithBFS(input);
			
			if(dfsTree!=null){
				BinaryTree randomChild=findRandomLastChild(dfsTree);
				System.out.println("Response = "+randomChild.getData());
				System.out.println(input+" found with "+dfsSteps+" DFS steps and "+bfsSteps+" wid BFS\n");
			}else{
				System.out.println("Cannot find "+input+"\n");
			}
			System.out.print("'quit' to exit: ");
			input = reader.nextLine();
		}while(!input.equalsIgnoreCase("exit") || !input.equalsIgnoreCase("quit"));
	}
	
	BinaryTree searchWithDFS(BinaryTree t,String n){
		dfsSteps++;
		if(n.equalsIgnoreCase(t.getData())){
			return t;
		}else{
			for(int i=0; i<t.totalNumberOfChildren(); i++ ){
				BinaryTree tree = searchWithDFS(t.getChild(i),n);
				if(tree!=null) {
					return tree;
				}
			}
		}
		return null;
	}
	
	BinaryTree searchWithBFS(String response){
		bfsSteps++;
		if(bfsQ.peek().getData().equalsIgnoreCase(response)){
			return bfsQ.peek();
		}
		while(!bfsQ.isEmpty()){
			for(int i=0; i<bfsQ.peek().totalNumberOfChildren(); i++){
				bfsSteps++;
				if(bfsQ.peek().getChild(i).getData().equalsIgnoreCase(response))
					return bfsQ.peek().getChild(i);
				bfsQ.add(bfsQ.peek().getChild(i));
			}
			bfsQ.poll();
		}
		
		return null;
		
	}
	
	BinaryTree findRandomLastChild(BinaryTree t){
		if(t.totalNumberOfChildren()==0) return t;
		Random rand = new Random();
		int  random = rand.nextInt(t.totalNumberOfChildren());
		return findRandomLastChild(t.getChild(random));
		
	}
	
}
