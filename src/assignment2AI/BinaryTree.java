package assignment2AI;

import java.util.ArrayList;
import java.util.List;

public class BinaryTree {
	String data="";
	private List<BinaryTree> children = new ArrayList<BinaryTree>();
	
	void setData(String data){
		this.data=data;
	}
	
	void addChild(BinaryTree c){
		children.add(c);
	}
	
	String getData(){
		return this.data;
	}
	
	List<BinaryTree> getAllChildren(){
		return this.children;
	}
	
	BinaryTree getChild(int i){
		assert(children.size()<i);
		return children.get(i);
	}
	
	int totalNumberOfChildren(){
		return children.size();
	}
	
	Boolean isempty(){
		return data.equals("");
	}
	
}
